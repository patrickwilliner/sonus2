import {
  Component,
  OnDestroy,
  OnInit,
  computed,
  effect,
  inject,
  signal,
} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { MenuComponent } from './components/menu/menu.component';
import { PlayerComponent } from './components/player/player.component';
import { PlaylistComponent } from './components/playlist/playlist.component';
import { PlayerService } from './services/player.service';
import { PlaylistService } from './services/playlist.service';
import { EMPTY_PLAYLIST, Playlist } from './models/playlist.model';
import { Subscription, first, tap, switchMap } from 'rxjs';
import { PlayerStatus } from './models/player.model';
import { StorageService } from './services/storage.service';
import { ProgressComponent } from './components/progress/progress.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, MenuComponent, PlayerComponent, PlaylistComponent, ProgressComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit, OnDestroy {
  private playerService = inject(PlayerService);
  private playlistService = inject(PlaylistService);
  private storageService = inject(StorageService);

  private subscriptions: Subscription[] = [];

  title = 'Sonus Music Player';
  playlist = signal<Playlist>(EMPTY_PLAYLIST);
  playerStatus = signal(PlayerStatus.STOPPED);
  isPlaying = computed(() => this.playerStatus() === PlayerStatus.PLAYING);
  timer = signal<number>(0);

  tmr = computed(() => {
    if (this.playlist().selected >= 0) {
      let currentSong = this.playlist().songs[this.playlist().selected];
      let val = 100 * this.timer() / currentSong.meta.duration;
      return val;
    } else {
      return 0;
    }
  });

  constructor() {
    this.subscribePlaylistService();
    this.subscribePlayerService();

    effect(
      () => {
        if (this.playlist().selected >= 0) {
          let song = this.playlistService.getSong();

          if (song != null) {
            this.playerService.play(song);
          }
        }
      },
      { allowSignalWrites: true },
    ); // TODO: maybe can be removed now
  }

  ngOnInit(): void {
    this.storageService.loadPlaylist().pipe(
      first(),
      tap(playlist => this.playlistService.setPlaylist(playlist))
    ).subscribe(); // TODO unsubscribe
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((s) => s.unsubscribe);
  }

  onLoadFiles(files: File[]): void {
    this.playerService.stop();
    this.playlistService.loadFiles(files).pipe(
      first(),
      switchMap(playlist => this.storageService.savePlaylist(playlist))
    ).subscribe(); // TODO unsubscribe
  }

  onChangeSelectedSong(index: number): void {
    this.playlistService.select(index);
  }

  onClickPlay(): void {
    let song = this.playlistService.getSong();

    if (song != null) {
      this.playerService.play(song);
    }
  }

  onClickPause(): void {
    this.playerService.pause();
  }

  onClickNext(): void {
    this.playlistService.next();
  }

  onClickPrevious(): void {
    this.playlistService.previous();
  }

  private subscribePlaylistService(): void {
    this.subscriptions.push(
      this.playlistService.getPlaylist().pipe(
        tap(playlist => playlist != null && this.playlist.set(playlist))
      ).subscribe()
    );
  }

  private subscribePlayerService(): void {
    this.subscriptions.push(
      this.playerService
        .getPlayerStatus()
        .subscribe((playerStatus) => this.playerStatus.set(playerStatus)),
    );

    this.subscriptions.push(
      this.playerService.getTimer().pipe(
      ).subscribe(
        time => this.timer.set(time)
      )
    );
  }
}
