export type Meta = {
  title: string;
  artist: string;
  album: string;
  track: number;
  duration: number;
  year: string;
  picture?: number[];
  tagVersion: string;
};

export type Song = {
  file: File;
  meta: Meta;
};
