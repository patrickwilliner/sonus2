import { Song } from './song.model';

export type Playlist = {
  songs: Song[];
  selected: number;
};

export const EMPTY_PLAYLIST: Playlist = Object.freeze({
  songs: [],
  selected: -1,
});
