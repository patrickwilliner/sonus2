import { Pipe, PipeTransform } from '@angular/core';
import { FormatUtil } from '../lib/util/format.util';

@Pipe({
  name: 'duration',
  standalone: true,
})
export class DurationPipe implements PipeTransform {
  transform(duration: number): string {
    return FormatUtil.duration(duration);
  }
}
