import { Injectable, OnInit } from '@angular/core';
import { EMPTY_PLAYLIST, Playlist } from '../models/playlist.model';
import {
  BehaviorSubject,
  Observable,
  combineLatest,
  first,
  forkJoin,
  fromEvent,
  map,
  take,
  tap,
} from 'rxjs';
import { Song } from '../models/song.model';
import MetaDataParser from '../lib/metadata/meta-data-parser';

@Injectable({
  providedIn: 'root',
})
export class PlaylistService implements OnInit {
  private playlist = new BehaviorSubject<Playlist>(EMPTY_PLAYLIST);

  constructor() {}

  ngOnInit(): void {}

  getSong(): Song | undefined {
    let playlist = this.playlist.getValue();
    return playlist.songs[playlist.selected];
  }

  getPlaylist(): Observable<Playlist> {
    return this.playlist.asObservable();
  }

  setPlaylist(playlist: Playlist): void {
    this.playlist.next(playlist);
  }

  loadFiles(files: File[]): Observable<Playlist> {
    return this.createPlaylist(files)
      .pipe(
        take(1),
        tap((playlist) => this.playlist.next(playlist)),
      );
  }

  previous(): void {
    let playlist = this.playlist.getValue();

    if (playlist.selected > 0) {
      this.playlist.next({
        ...playlist,
        selected: playlist.selected - 1,
      });
    }
  }

  next(): void {
    let playlist = this.playlist.getValue();

    if (playlist.selected < playlist.songs.length - 1) {
      this.playlist.next({
        ...playlist,
        selected: playlist.selected + 1,
      });
    }
  }

  select(index: number): void {
    let playlist = this.playlist.getValue();
    this.playlist.next({
      ...playlist,
      selected: index,
    });
  }

  private createPlaylist(files: File[]): Observable<Playlist> {
    let observables = files.map((file) => this.createSong(file));
    return forkJoin(observables).pipe(
      map((songs) => ({ songs, selected: -1 })),
    );
  }

  private createSong(file: File): Observable<Song> {
    let metaParser = new MetaDataParser();
    let song$ = metaParser.parse(file).pipe(map((meta) => ({ meta, file })));

    let duration$ = this.getDuration(file);

    return combineLatest([song$, duration$]).pipe(
      map(([song, duration]) => {
        song.meta.duration = duration;
        return song;
      }),
    );
  }

  private getDuration(file: File): Observable<number> {
    const url = window.URL.createObjectURL(file);
    const audio = new Audio(url);

    audio.onerror = () => {
      //this.errorHandlerService.handle(`Could not load "${acc.file.name}"`);
    };

    return fromEvent(audio, 'loadedmetadata').pipe(
      first(),
      map(() => audio.duration),
      map(Math.floor),
    );
  }
}
