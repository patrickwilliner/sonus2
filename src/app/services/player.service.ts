import { Injectable, OnDestroy } from '@angular/core';
import { Song } from '../models/song.model';
import {
  BehaviorSubject,
  Observable,
  Subscription,
  distinct,
  first,
  fromEvent,
  interval,
  map,
  tap,
} from 'rxjs';
import { PlayerStatus } from '../models/player.model';

const TIMER_INTERVAL_MS = 1000;

@Injectable({ providedIn: 'root' })
export class PlayerService implements OnDestroy {
  private playerStatus$ = new BehaviorSubject<PlayerStatus>(
    PlayerStatus.STOPPED,
  );
  private timer$ = new BehaviorSubject<number>(0);
  private audio: HTMLAudioElement | undefined;

  private subscriptions: Subscription[] = [];
  private timerSubscription: Subscription|undefined;

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe();
    });
  }

  play(song: Song): void {
    let fileUrl = URL.createObjectURL(song.file);
    this.createNewAudio(fileUrl);

    if (this.audio != null) {
      this.audio.play();
      this.playerStatus$.next(PlayerStatus.PLAYING);
      this.startTimer();
      let songHasEnded$ = fromEvent(this.audio, 'ended').pipe(
        map(() => undefined),
      );
      songHasEnded$
        .pipe(
          first(),
          tap(() => this.playerStatus$.next(PlayerStatus.STOPPED)),
        )
        .subscribe();
    }
  }

  pause(): void {
    this.audio?.pause();
    this.playerStatus$.next(PlayerStatus.PAUSED);
    this.stopTimer();
  }

  stop(): void {
    this.audio?.pause();
    delete this.audio;
  }

  getPlayerStatus(): Observable<PlayerStatus> {
    return this.playerStatus$.asObservable();
  }

  getTimer(): Observable<number> {
    return this.timer$.asObservable();
  }

  private startTimer(): void {
    this.timerSubscription = interval(TIMER_INTERVAL_MS).pipe(
      map(() => this.audio?.currentTime ?? 0),
      map(Math.floor),
      distinct(),
      tap(time => console.log('time:', time))
    ).subscribe(this.timer$);
  }

  private stopTimer(): void {
    this.timerSubscription?.unsubscribe();
  }

  private createNewAudio(playableUrl: string): HTMLAudioElement {
    this.destroyAudio();
    this.audio = new Audio(playableUrl);
    this.audio.volume = 1;
    return this.audio;
  }

  private destroyAudio(): void {
    if (this.audio != null) {
      this.audio.pause();
      this.audio.load();
      delete this.audio;
    }
  }
}
