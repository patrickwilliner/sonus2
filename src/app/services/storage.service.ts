import { Injectable } from '@angular/core';
import { combineLatest, from, Observable, Subject } from 'rxjs';
import { first, map, mergeMap, switchMap, tap, toArray } from 'rxjs/operators';
import { Playlist } from '../models/playlist.model';

const DB_NAME = 'music_data';
const DB_VERSION = 1;
const OBJECTSTORE_SESSION = 'session'
const OBJECTSTORE_SESSION_FILES = 'session_files'
const OBJECTSTORE_LIBRARY = 'library'

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor() { }

  storeFile(file: File): Observable<string> {
    return this.getDbRequest().pipe(
      switchMap((event: any) => {
        const db: IDBDatabase = event.target['result'];
        const transaction = db.transaction(OBJECTSTORE_SESSION_FILES, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_SESSION_FILES);
        return this.asObservable(os.add(file));
      }),
      map((event: any) => event.target['result'])
    );
  }

  savePlaylist(playlist: Playlist): Observable<Playlist> {

    return this.getDbRequest().pipe(
      switchMap((event: any) => {
        const db: IDBDatabase = event.target['result'];
        const transaction = db.transaction(OBJECTSTORE_SESSION, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_SESSION);
        console.log('...', playlist);
        return this.asObservable(os.put(playlist, 1));
      }),
      map((event: any) => {
        return {
          ...playlist,
          id: event.target['result']
        };
      })
    );
  }

  loadPlaylist(): Observable<Playlist> {
    return this.getDbRequest().pipe(
      switchMap((event: any) => {
        const db: IDBDatabase = event.target['result'];
        const transaction = db.transaction(OBJECTSTORE_SESSION, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_SESSION);
        return this.asObservable(os.getAll());
      }),
      map((event: any) => event.target['result'][0])
    );
  }

  private getDbRequest(): Observable<Event> {
    const subject = new Subject<Event>();
    const request = indexedDB.open(DB_NAME, DB_VERSION);
    request.onupgradeneeded = this.createDatabaseSchema.bind(this);
    request.onsuccess = event => {
      subject.next(event);
      subject.complete();
    };
    request.onerror = error => {
      subject.error(error);
    };
    return subject.asObservable();
  }

  private createDatabaseSchema(event: any): void {
    const db: IDBDatabase = event.target['result'];
    db.createObjectStore(OBJECTSTORE_SESSION, { autoIncrement: true });
    db.createObjectStore(OBJECTSTORE_LIBRARY, { autoIncrement: true });
    db.createObjectStore(OBJECTSTORE_SESSION_FILES, { autoIncrement: true });
  }

  private asObservable(request: IDBRequest<any>): Observable<Event> {
    const subject = new Subject<any>();

    request.onsuccess = event => {
      subject.next(event);
      subject.complete();
    };

    request.onerror = error => {
      subject.error(error);
    };

    return subject.asObservable();
  }
}
