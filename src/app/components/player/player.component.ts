import {
  Component,
  EventEmitter,
  Input,
  Output,
  input,
  signal,
} from '@angular/core';

@Component({
  selector: 'app-player',
  standalone: true,
  imports: [],
  templateUrl: './player.component.html',
  styleUrl: './player.component.scss',
})
export class PlayerComponent {
  isPlaying = input.required<boolean>();

  @Output()
  clickPlay = new EventEmitter<void>();

  @Output()
  clickPause = new EventEmitter<void>();

  @Output()
  clickPrevious = new EventEmitter<void>();

  @Output()
  clickNext = new EventEmitter<void>();
}
