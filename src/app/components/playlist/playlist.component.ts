import { Component, EventEmitter, Output, input } from '@angular/core';
import { Playlist } from '../../models/playlist.model';
import { CommonModule } from '@angular/common';
import { DurationPipe } from '../../pipes/duration.pipe';

@Component({
  selector: 'app-playlist',
  standalone: true,
  templateUrl: './playlist.component.html',
  styleUrl: './playlist.component.scss',
  imports: [CommonModule, DurationPipe],
})
export class PlaylistComponent {
  playlist = input.required<Playlist>();

  @Output()
  selectedChange = new EventEmitter<number>();

  selectRow(row: number): void {
    this.selectedChange.emit(row);
  }
}
