import {
  Component,
  ElementRef,
  EventEmitter,
  Output,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-menu',
  standalone: true,
  imports: [],
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.scss',
})
export class MenuComponent {
  @ViewChild('fileInput')
  fileInputElement!: ElementRef<HTMLInputElement>;

  @Output()
  loadFiles = new EventEmitter<File[]>();

  onClick(): void {
    this.fileInputElement.nativeElement.click();
  }

  onFileInputChange(event: Event): void {
    let fileList = (event?.target as HTMLInputElement)?.files ?? new FileList();
    let files = this.mapFileListToFileArray(fileList);
    this.loadFiles.emit(files);
  }

  private mapFileListToFileArray(fileList: FileList): File[] {
    let files: File[] = [];

    for (let i = 0; i < fileList.length; i++) {
      let file = fileList.item(i);
      if (file != null) {
        files.push(file);
      }
    }

    return files;
  }
}
