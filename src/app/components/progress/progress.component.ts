import { AfterViewInit, Component, ElementRef, Input, ViewChild, effect, input } from '@angular/core';

@Component({
  selector: 'app-progress',
  standalone: true,
  imports: [],
  templateUrl: './progress.component.html',
  styleUrl: './progress.component.scss'
})
export class ProgressComponent {
  /*
   * Min 0, max 100.
   */
  val = input.required<number>();

  @ViewChild('progressbar')
  svgProgressBar!: ElementRef;

  @ViewChild('svg')
  svgElement!: ElementRef;

  constructor() {
    effect(() => {
      this.svgProgressBar.nativeElement.setAttribute('x2', this.val());
    });
  }

  onMouseClick(event: Event): void {
  }
}
