import { Meta } from '../../../models/song.model';
import { BinaryFileReader } from '../binary-file-reader';
import { MetaTagParser } from '../meta-tag-parser';

const META_TAG = 'ID3v1';

export default class Id3v1Parser implements MetaTagParser {
  constructor(private fileReader: BinaryFileReader) {}

  canParse(): boolean {
    const offset = this.fileReader.byteLength - 128;
    return this.fileReader.readString(offset, 3) === 'TAG';
  }

  parse(): Meta {
    const meta: Meta = {
      title: '',
      artist: '',
      album: '',
      track: -1,
      length: -1,
      year: '',
      picture: [],
      tagVersion: '',
    };

    let offset = this.fileReader.byteLength - 128 + 3;
    let title = (meta.title = this.fileReader.readString(offset, 30));

    offset += 30;
    let artist = (meta.artist = this.fileReader.readString(offset, 30));

    offset += 30;
    let album = (meta.album = this.fileReader.readString(offset, 30));

    offset += 30;
    let year = this.fileReader.readString(offset, 4);

    meta.tagVersion = META_TAG;

    return meta;
  }
}
