import { Meta } from '../../models/song.model';

export interface MetaTagParser {
  canParse(): boolean;
  parse(): Meta;
}
