import { Meta } from '../../../models/song.model';
import { BinaryFileReader } from '../binary-file-reader';
import { MetaTagParser } from '../meta-tag-parser';
import { Id3v22FrameReader } from './id3v22-frame-reader';
import { Id3v23FrameReader } from './id3v23-frame-reader';

const HEADER_SIZE = 10;

export class Id3v2Parser implements MetaTagParser {
  constructor(private fileReader: BinaryFileReader) {}

  // file starts with pattern $49 44 33 yy yy xx zz zz zz zz
  // where 'yy' < ff and 'zz' < 80 and xx is flags
  canParse(): boolean {
    return (
      this.fileReader.readByte(0) === 0x49 &&
      this.fileReader.readByte(1) === 0x44 &&
      this.fileReader.readByte(2) === 0x33 &&
      this.fileReader.readByte(6) < 0xff &&
      this.fileReader.readByte(7) < 0xff &&
      this.fileReader.readByte(8) < 0x80 &&
      this.fileReader.readByte(9) < 0x80
    );
  }

  parse(): Meta {
    const header = this.readHeader();
    const framesSectionLength = header['size'] - HEADER_SIZE;

    if (header['minorVersion'] > 2) {
      const frameReader = new Id3v23FrameReader(this.fileReader);
      let frames: any = frameReader.readFrames(
        HEADER_SIZE,
        framesSectionLength,
        header,
      );

      return {
        title: frames['TIT2']?.data,
        track: frames['TRCK']?.data,
        artist: frames['TPE1']?.data || frames['TPE2']?.data,
        album: frames['TALB']?.data,
        year: frames['TYER']?.data,
        picture: frames['APIC']?.data,
        duration: -1,
        tagVersion: 'id3v2',
      };
    } else {
      const frameReader = new Id3v22FrameReader(this.fileReader);
      let frames: any = frameReader.readFrames(
        HEADER_SIZE,
        framesSectionLength,
        header,
      );

      return {
        title: frames['TT2']?.data,
        track: frames['TRK']?.data,
        artist: frames['TP1']?.data || frames['TP2']?.data,
        album: frames['TAL']?.data,
        year: frames['TYE']?.data,
        picture: frames['PIC']?.data,
        duration: -1,
        tagVersion: 'id3v2',
      };
    }
  }

  private readHeader(): any {
    const header: any = {};

    // read version
    header['minorVersion'] = this.fileReader.readByte(3);
    header['revisionVersion'] = this.fileReader.readByte(4);

    // read flags
    const flagByte = this.fileReader.readByte(5);
    header['flags'] = {};
    header['flags']['unsynchronization'] = this.fileReader.isBitSet(
      flagByte,
      7,
    );
    header['flags']['extendedHeader'] = this.fileReader.isBitSet(flagByte, 6);
    header['flags']['experimental'] = this.fileReader.isBitSet(flagByte, 5);

    // read size
    header['size'] = this.fileReader.readNumber(6, 4, 7);

    // extended header
    header['extendedHeader'] = this.readExtendedHeader();

    return header;
  }

  private readExtendedHeader(): any {}
}
