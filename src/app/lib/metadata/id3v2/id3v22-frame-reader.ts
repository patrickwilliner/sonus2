import { Id3v2FrameReaderAbs } from './id3v2FrameReaderAbs';

export class Id3v22FrameReader extends Id3v2FrameReaderAbs {
  readFrames(offset: number, length: number, header: any): any {
    const result: any = {};

    if (header['flags']['extendedHeader']) {
      offset += header['extendedHeader']['size'];
    }

    while (offset < length) {
      let frameIdLength: number;
      let frameSize: number;
      let frameHeaderSize: number;

      frameIdLength = 3;
      frameSize = this.fileReader.readNumber(offset + 3, 3);
      frameHeaderSize = 6;

      const frameId = this.fileReader.readString(offset, frameIdLength);

      if (frameId !== '') {
        const frameFlags = this.readFrameFlags(offset + 4 + 4);

        offset += frameHeaderSize;

        let frameData: any;
        if (frameId.toLocaleLowerCase().indexOf('t') === 0) {
          frameData = this.readFrameDataAsText(offset, frameSize);
        }

        if (frameId.toLowerCase() === 'pic') {
          frameData = this.readFrameDataAsPicture(offset, frameSize, header);
        }

        result[frameId] = {
          size: frameSize,
          frameFlags: frameFlags,
          data: frameData,
        };

        offset += frameSize;
      } else {
        break;
      }
    }

    return result;
  }

  private readFrameDataAsPicture(
    offset: number,
    dataLength: number,
    header: any,
  ): any {
    const origOffset = offset;
    const charset = this.readTextCharset(offset);

    // MIME type: <text string> $00
    offset += 1;
    const mimeType = this.fileReader.readString(offset, dataLength);

    offset += 4;

    const pictureType = this.fileReader.readByte(offset);

    offset += 1;

    const pictureData = this.fileReader.readBytes(offset, dataLength);

    return {
      pictureType: pictureType,
      mimeType: mimeType,
      charset: charset,
      data: pictureData,
    };
  }
}
