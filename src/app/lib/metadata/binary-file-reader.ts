import { Buffer } from 'buffer';
import { fromEvent, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export class BinaryFileReader {
  private constructor(
    private dataView: DataView,
    private file: File,
  ) {}

  static create(file: File, bufferSize: number): Observable<BinaryFileReader> {
    const fileReader = new FileReader();
    fileReader.readAsArrayBuffer(file);

    return fromEvent(fileReader, 'load').pipe(
      map((event) => {
        let result =
          event.target != null ? (event.target as any)['result'] : null;
        if (result != null) {
          return new DataView(result);
        } else {
          return new DataView(new ArrayBuffer(0));
        }
      }),
      map((dataView) => new BinaryFileReader(dataView, file)),
    );
  }

  readString(offset: number, length: number): string {
    const data = [];
    for (let i = offset; i < offset + length; i++) {
      const byte = this.dataView.getUint8(i);
      if (byte === 0) {
        break;
      }
      data.push(byte);
    }
    return new Buffer(data).toString();
  }

  readStringUtf8(offset: number, length: number): string {
    const data = [];
    for (let i = offset; i < offset + length; i++) {
      const firstByte = this.dataView.getUint8(i);
      data.push(firstByte);
    }
    return new Buffer(data).toString('utf8');
  }

  readStringUtf16(offset: number, length: number): string {
    // skip unicode bom
    offset = offset + 2;
    length = length - 2;

    const data = [];
    for (let i = offset; i < offset + length; i += 2) {
      const firstByte = this.dataView.getUint8(i);
      const secondByte = this.dataView.getUint8(i + 1);

      if (firstByte === 0 && secondByte === 0) {
        break;
      }

      data.push(firstByte);
      data.push(secondByte);
    }
    return new Buffer(data).toString('utf16le');
  }

  readByte(offset: number): number {
    return this.dataView.getUint8(offset);
  }

  readBytes(offset: number, length: number): number[] {
    const result = [];
    for (let i = 0; i < length; i++) {
      result.push(this.readByte(offset + i));
    }

    return result;
  }

  readNumber(
    offset: number,
    bytes: number,
    base = 8,
    isBigEndian = true,
  ): number {
    let result = 0;

    if (isBigEndian) {
      for (let i = bytes; i > 0; i--) {
        result =
          result |
          ((this.readByte(offset + i - 1) & (Math.pow(2, base) - 1)) <<
            ((bytes - i) * base));
      }
    } else {
      for (let i = 0; i < bytes; i++) {
        result =
          result |
          ((this.readByte(offset + i) & (Math.pow(2, base) - 1)) >> (i * base));
      }
    }

    return result;
  }

  isBitSet(byte: number, bit: number): boolean {
    return (byte & (1 << bit)) === 1;
  }

  get byteLength(): number {
    return this.dataView.byteLength;
  }
}
