import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { BinaryFileReader } from './binary-file-reader';
import { Id3v2Parser } from './id3v2/id3v2-parser';
import { FlacParser } from './flac/flac-parser';
import { Meta } from '../../models/song.model';

export default class MetaDataParser {
  parse(file: File): Observable<Meta> {
    return BinaryFileReader.create(file, 1024)
      .pipe(first())
      .pipe(map((fileReader) => this.callParser(fileReader)));
  }

  private callParser(fileReader: BinaryFileReader): Meta {
    const id3v2Parser = new Id3v2Parser(fileReader);
    if (id3v2Parser.canParse()) {
      return id3v2Parser.parse();
    } else {
      const flacParser = new FlacParser(fileReader);
      if (flacParser.canParse()) {
        return flacParser.parse();
      } else {
        return {
          album: '',
          artist: '',
          duration: -1,
          tagVersion: '',
          title: '',
          track: -1,
          year: '',
        };
      }
    }
  }
}
