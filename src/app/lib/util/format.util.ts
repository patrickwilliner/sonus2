export class FormatUtil {
  static duration(seconds: number): string {
    return seconds != null ? this.format(seconds) : '';
  }

  private static format(seconds: number): string {
    const secondsRemainder = seconds % 60;
    const minutes = (seconds - secondsRemainder) / 60;
    return `${this.pad(minutes, 2)} : ${this.pad(secondsRemainder, 2)}`;
  }

  private static pad(value: number, width: number): string {
    const z = '0';
    const valueString = value + '';

    if (valueString.length >= width) {
      return valueString;
    } else {
      return new Array(width - valueString.length + 1).join(z) + valueString;
    }
  }
}
